dockercompose-flask-redis
---

A simple example of using Docker Compose with a Flask application, a background worker, and Redis as a shared memorystore.

### Setup

This requires [Docker](https://docs.docker.com/install/) and [`docker-compose`](https://docs.docker.com/compose/install/).

### Building and running

To get the services running quickly, just run

```bash
$ docker-compose up
```

If you've made changes and want to rebuild everything from scratch you can run

```bash
$ docker-compose build && docker-compose up --force-recreate
```

If you want to start the services headlessly, you can run

```bash
$ docker-compose up -d
```